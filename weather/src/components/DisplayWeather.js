import React, { useState } from 'react';
import axios from 'axios'
import {countries} from 'country-data';
import "./weather.css";
import DisplayCountryData from "./DisplayCountryData";

const api = {
  key: process.env.REACT_APP_WEATHER_API_KEY,
  base: "https://api.openweathermap.org/data/2.5/"
}

function DisplayWeather() {
  // Declare new state variables
  const [data, setData] = useState({})
  const [location, setLocation] = useState('')

  const url = `${api.base}weather?q=${location}&units=metric&appid=${api.key}`

  const search = evt => {
    /*if (evt.key === "Enter") {
      // query get request using fetch
      fetch(url)
          // get the json promise from the response
          .then(res => res.json())
          // pass the json response to another next promise
          .then(result => {
            setData(result);
            setLocation('');
            console.log(result);
          });
    }*/

      if (evt.key === 'Enter') {
          // query get request using axios
          axios.get(url)
              .then((response) => {
              setData(response.data)
              console.log(response.data)
          })
          setLocation('')
      }
  }

  const dateBuilder = (d) => {
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    let day = days[d.getDay()];
    let date = d.getDate();
    let month = months[d.getMonth()];
    let year = d.getFullYear();

    return `${day} ${date} ${month} ${year}`
  }

  return (
      <div className={(typeof data.main != "undefined") ? ((data.main.temp > 16) ? 'app warm' : 'app') : 'app'}>
        <main>
          <div className="search-box">
            <input
                type="text"
                className="search-bar"
                placeholder="Search..."
                // evt to get the value of input
                onChange={e => setLocation(e.target.value)}
                value={location}
                onKeyPress={search}
            />
          </div>
          {(typeof data.main != "undefined") ? (
              <div className="container">
                  <div className="top">
                      <div className="location-box">
                          <div className="location">
                              {data.name}, {countries[data.sys.country].name}
                          </div>
                          <div className="date">
                              {dateBuilder(new Date())}
                          </div>
                      </div>
                      <div className="weather-box">
                          <div className="temp">
                              {data.main ? <h1>{Math.round(data.main.temp)}°c</h1> : null}
                          </div>
                          <div className="description">
                              {data.weather ? <p>{data.weather[0].main}</p> : null}
                          </div>
                      </div>
                  </div>

                  <div className="bottom">
                      <div className="feels">
                          <p>Feels Like</p>
                          {data.main ? <p className='bold'>{data.main.feels_like.toFixed()}°F</p> : null}
                      </div>
                      <div className="humidity">
                          <p>Humidity</p>
                          {data.main ? <p className='bold'>{data.main.humidity}%</p> : null}
                      </div>
                      <div className="wind">
                          <p>Wind Speed</p>
                          {data.wind ? <p className='bold'>{data.wind.speed.toFixed()} MPH</p> : null}
                      </div>
                  </div>

                  <div>
                      <DisplayCountryData countryCode={data.sys.country} />
                  </div>

              </div>
          ) : ('')}
        </main>
      </div>
  );
}

export default DisplayWeather;