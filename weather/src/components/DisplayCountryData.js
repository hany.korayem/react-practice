import React from 'react';
import {countries} from 'country-data';
import "./country-data.css";

function DisplayCountryData(props) {
    const { countryCode } = props;
    console.log(countryCode)
    return (
          <div className="country-data">
              {(typeof countryCode != "undefined") ? (
                  <React.Fragment>

                      <div className="currency">
                          <p>Currency</p>
                          {countryCode ? <p className='bold'>{countries[countryCode].currencies[0]}</p> : null}
                      </div>
                      <div className="languages">
                          <p>Languages</p>
                          {countryCode ? <p className='bold'>{countries[countryCode].languages[0]}</p> : null}
                      </div>
                      <div className="calling-code">
                          <p>Calling Code</p>
                          {countryCode ? <p className='bold'>{countries[countryCode].countryCallingCodes[0]}</p> : null}
                      </div>
                </React.Fragment>
              ) : ('')}
          </div>
    );
}

export default DisplayCountryData;